//
//  ClassA.h
//  DemoUnitTesting
//
//  Created by Finskii on 8/19/15.
//  Copyright (c) 2015 Finskii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassA : UIViewController

@property int primitive1;
@property int primitive2;

-(void)initPrimitives;

@end
