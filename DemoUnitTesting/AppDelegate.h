//
//  AppDelegate.h
//  DemoUnitTesting
//
//  Created by Finskii on 8/19/15.
//  Copyright (c) 2015 Finskii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

