//
//  ClassA.m
//  DemoUnitTesting
//
//  Created by Finskii on 8/19/15.
//  Copyright (c) 2015 Finskii. All rights reserved.
//

#import "ClassA.h"

@interface ClassA ()

@end

@implementation ClassA

- (void)viewDidLoad {
    [super viewDidLoad];

    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initPrimitives
{
    self.primitive1 = 9;
    self.primitive2 = 9;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
