//
//  DemoUnitTestingTests.m
//  DemoUnitTestingTests
//
//  Created by Finskii on 8/19/15.
//  Copyright (c) 2015 Finskii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "ClassA.h"
#import "ClassB.h"

@interface DemoUnitTestingTests : XCTestCase

@end

@implementation DemoUnitTestingTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testIsNotEqualPrimitive {
    /*
     аргумент1 - примитив1
     аргумент2 - примитив2
     необязательно
     аргумент3 - формат вывода
     последующие - аргументы в формат для вывода ошибки
     */
    ClassA* classA = [ClassA new];
    [classA initPrimitives];
    XCTAssertNotEqual(classA.primitive1, classA.primitive2, @"(%d) not equal to (%d)", classA.primitive2, classA.primitive2);
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
